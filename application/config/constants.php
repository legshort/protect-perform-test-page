<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('DEVELOPMENT', 'development');
define('PRODUCTION', 'production');
define('TESTING', 'testing');

if (ENVIRONMENT === DEVELOPMENT)
{
	define('LOG_THRESHOLD', 4);
	define('DB_DEBUG', TRUE);
	define('DB_TEST', TRUE);
	ini_set('display_errors', TRUE);
}
if (ENVIRONMENT === TESTING)
{
	define('LOG_THRESHOLD', 0);
	define('DB_DEBUG', TRUE);
	define('DB_TEST', FALSE);
	ini_set('display_errors', TRUE);
}
else if (ENVIRONMENT === PRODUCTION)
{
	define('LOG_THRESHOLD', 1);
	define('DB_DEBUG', TRUE);
	define('DB_TEST', FALSE);
	ini_set('display_errors', TRUE);
}

define('MYSQL_DATETIME_FORMAT', 'Y-m-d H:i:s');
define('MYSQL_DATE_FORMAT', 'Y-m-d');

define('NULL_STR', '');
define('SPACE', ' ');

/* End of file constants.php */
/* Location: ./application/config/constants.php */