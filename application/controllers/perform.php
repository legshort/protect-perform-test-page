<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Perform extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function test()
	{
		$this->load->view('perform/perform');
	}

	public function before()
	{
		$this->load->view('perform/before');
	}

	public function after()
	{
		$this->load->view('perform/after');
	}

	public function y01()
	{
		$this->load->view('perform/y01/y01');
	}

	public function y03()
	{
		$this->load->view('perform/y03/y03');
	}

	public function y08()
	{
		$this->load->view('perform/y08/y08');
	}

	public function y11()
	{
		$this->load->view('perform/y11/y11');
	}

	public function y13()
	{
		$this->load->view('perform/y13/y13');
	}

	public function y17()
	{
		$this->load->view('perform/y17/y17');
	}
}

/* End of file protect.php */
/* Location: ./application/controllers/protect.php */