<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Protect extends CI_Controller
{
	private $request_method;

	public function __construct()
	{
		parent::__construct();

		$this->request_method = $_SERVER['REQUEST_METHOD'];
		$this->load->helper('url');
	}

	public function test()
	{
		redirect(base_url('protect/test_fail?sessionId=18fgr982y4lk2ouhyf04ehy4028hyehyf'));
	}

	public function test_fail()
	{
		$this->load->view('protect/a2/a2');
	}

	public function pnp()
	{
		// a2
		redirect(base_url('protect/pnp_fail?sessionId=18fgr982y4lk2ouhyf04ehy4028hyehyf'));
	}

	public function pnp_fail()
	{
		$this->load->view('protect/pnp');
	}

	public function a3()
	{
		$this->load->view('protect/a3/a3.php');
	}

	public function a3_post()
	{
		$this->load->helper('html');
		$table = 'post';

		$path = $this->input->get_request_header('User-agent');

		// a4
		if (strpos($path, 'system.ini') !== false || strpos($path, 'etc/password') !== false || strpos($path, 'web.xml') !== false) {
			$this->load->helper('file');
			$this->load->helper('download');

			$string = read_file($path);
			$file_info = get_file_info($path);

			$file_name = basename($file_info['server_path']);

			force_download($file_name, $string);
			// a10
		} else if (strpos($_POST['post_content'], 'http://protectnperform.com') !== false) {
			redirect($_POST['post_content']);
			//
		} else if ($this->request_method === 'GET') {
			$post_content = $_GET['post_content'];

			$con = mysqli_connect('localhost', 'pnp', 'tkgkrsus1!', 'pnp');
			// Check connection
			if (mysqli_connect_errno()) {
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}

			$query = 'SELECT * FROM `post` WHERE `post_id`=' . $post_content;
			$result = mysqli_query($con, $query);

			while ($row = mysqli_fetch_array($result)) {
			}
		} else if ($this->request_method === 'POST') {
			$post_content = $_POST['post_content'];

			$this->db->insert($table, array('post_content' => $post_content));
			$post_id = $this->db->insert_id();
			$array_where = array('post_id' => $post_id);
			$post = $this->db->get_where($table, $array_where)->row();

			echo br();
			echo 'Content: ';
			echo urldecode($post->post_content);
			echo br();

			$post_content = $this->input->get_request_header('User-agent');
			$this->db->insert($table, array('post_content' => $post_content));
			$post_id = $this->db->insert_id();
			$array_where = array('post_id' => $post_id);
			$post = $this->db->get_where($table, $array_where)->row();

			echo br();
			echo 'Content: ';
			echo urldecode($post->post_content);
			echo br();

			$post_content = $this->input->get_request_header('Host');
			$this->db->insert($table, array('post_content' => $post_content));
			$post_id = $this->db->insert_id();
			$array_where = array('post_id' => $post_id);
			$post = $this->db->get_where($table, $array_where)->row();

			echo br();
			echo 'Content: ';
			echo urldecode($post->post_content);
		}
	}

	public function a4()
	{
		$this->load->view('protect/a4/a4');
	}

	public function a4_echo()
	{
		// http://pnp.com/protect/a4_echo?protectnperform=%5CWindows%5Csystem.ini
		$this->load->helper('file');

		$path = ($this->request_method === 'GET') ? $_GET['file'] : $_POST['file'];
		$string = read_file($path);

		echo $string;

		$path = $this->input->get_request_header('User-agent');
		$string = read_file($path);
		echo $string;

		$path = $this->input->get_request_header('Host');
		$string = read_file($path);
		echo $string;
	}

	public function a4_download()
	{
		// http://pnp.com/protect/a4_download?protectnperform=%5CWindows%5Csystem.ini
		$this->load->helper('file');
		$this->load->helper('download');

		$path = $this->input->get_request_header('User-agent');
		$string = read_file($path);
		$file_info = get_file_info($path);

		force_download($file_info['name'], $string);
	}

	public function a10()
	{
		$this->load->view('protect/a10/a10');
	}

	public function a10_post()
	{
		$url = ($this->request_method === 'GET') ? $_GET['protectnperform'] : $_POST['url'];
		redirect($url);
	}
}

/* End of file protect.php */
/* Location: ./application/controllers/protect.php */