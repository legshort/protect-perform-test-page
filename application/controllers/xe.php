<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Xe extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->_checkUri();
	}

	private function _checkUri()
	{
		$uri = $this->uri->uri_string();

		if ($uri === 'xe/module/install/install.php')
		{
			$this->output->set_status_header('200');
			exit();
		}
	}
}

/* End of file protect.php */
/* Location: ./application/controllers/protect.php */