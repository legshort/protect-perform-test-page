<!DOCTYPE html>
<html>
<head>
	<title>Y01</title>

	<!-- 
	
	START : Y01 HEAD
	
	-->
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Js include -->
	<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="y01.js"></script>

	<!-- Js include with parameter -->
	<script src="http://code.jquery.com/jquery-1.10.2.min.js?20131016"></script>

	<!-- Js include without protocol -->
	<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>

	<!-- Js include comment -->
	<!-- <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script> -->

	<!-- Js include with bad url -->
	<script src="/code.jquery.com/jquery-1.10.2.min.js"></script>

	<!-- Duplicate Js include -->
	<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>


	<!-- Css include -->
	<link rel="stylesheet" type="text/css" href="http://getbootstrap.com/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="y01.css">

	<!-- Css include with Parameter -->
	<link rel="stylesheet" type="text/css" href="/css/carousel.css?20131016">

	<!-- Css include without protocol -->
	<link rel="stylesheet" type="text/css" href="/css/jquery.treeview.css">

	<!-- Css include comment -->
	<!--<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">-->

	<!-- Css include with bad url -->
	<link rel="stylesheet" type="text/css" href="themes/base/jquery-ui.css">

	<!-- Duplicate Css include -->
	<link rel="stylesheet" type="text/css" href="/css/carousel.css?20131016">

	<style>
		/* Css Sprite */

		#header .search .search-cate .list .cate a{font-weight:bold; background:#f5f5f5; background-image:none}
		#header .search .search-cate .list .cate a:hover{background-image:none}
		#header .cateWarp h2.typeB a{width:170px; height:38px; background-position:-500px -100px}/* Main CPP */

		/* Css InLine Image */
		.home { background-image: url(data:image/gif;base64,R0lGODlhHwAfAPcAAAAAAIxKA);}
		.gift { background-image: url(data:image/gif;base64,R0lGODlhHwAfAPcAAAAAAABCp);}
		.cart { background-image: url(data:image/gif;base64,R0lGODlhHwAfAPcAAAAAADlCr);}
	</style>
	
	<!-- 
	
	END : Y01 HEAD
	
	-->
	
	<!-- 
	
	START : Y03 HEAD
	
	-->
	
	<script src="y03.js"></script>
	
	<script type="text/javascript">
		img.src="";
		img.src='';
		img.src=null;
		img.src="null";
		img.src='null';
	</script>
	
	<!-- 
	
	END : Y03 HEAD
	
	-->
	
	<!-- 
	
	START : Y08 HEAD
	
	-->
	
	<link rel="stylesheet" type="text/css" href="y08.css">
	<style>
		width: expression( document.body.clientWidth < 600 ? "600px" : "auto" );
		min-width: 600px;

		P {
		width: expression( setCntr( ), document.body.clientWidth<600 ? "600px" : "auto" );
		min-width: 600px;
		border: 1px solid;
		}
	</style>

	
	<!-- 
	
	END : Y08 HEAD
	
	-->
	
	<!-- 
	
	START : Y11 HEAD
	
	-->
	
	<script src="y11.js"></script>
	<link rel="stylesheet" type="text/css" href="y11.css">
	
	<!-- 
	
	END : Y11 HEAD
	
	-->
	
	<!-- 
	
	START: Y13 HEAD
	
	-->
	
	<!-- Duplicate Js -->
	<script src="http://pnp.com/perform/y13.js"></script>
	<script src="//pnp.com/perform/y13.js"></script>
	<script src="/y13.js"></script>
	<script src="y13.js"></script>	
	
	<!-- Duplicate Css -->
	<link rel="stylesheet" type="text/css" href="http://pnp.com/perform/y13.css">
	<link rel="stylesheet" type="text/css" href="//pnp.com/perform/y13.css">
	<link rel="stylesheet" type="text/css" href="/y13.css">
	<link rel="stylesheet" type="text/css" href="y13.css">
	
	<!-- 
	
	END : Y13 HEAD
	
	-->
</head>

<body>

	<!-- 
	
	START : Y01 BODY
	
	-->
<!-- Image map -->
<map name="ko_map">
	<area shape="rect" coords="40,151,189,175" href="javascript:fnClickGlobalPopup('KO');" target="_self">
	<area shape="rect" coords="40,218,189,238" href="http://shop.gmarket.co.kr/oversea/OverseaMain.asp" target="_self">
	<area shape="rect" coords="64,245,166,258" href="http://www.gmarket.co.kr/challenge/neo_help/oversea/oversea_help.asp" target="_self">

	<!-- <area shape="rect" coords="40,151,189,175" href="javascript:fnClickGlobalPopup('KO');" target="_self"> -->
</map>

	<!-- 
	
	END : Y01 BODY
	
	-->
	
	<!-- 
	
	START : Y03 BODY
	
	-->
	
	<img src="">
	<img src=''>
	<img src=null>
	<img src="null">
	<img src='null'>

	<a href="" > </a>
	<a href='' > </a>
	<a href=null > </a>
	<a href="null" > </a>
	<a href='null' > </a>

	<!-- 
	
	END : Y03 BODY
	
	-->
</body>

</html>