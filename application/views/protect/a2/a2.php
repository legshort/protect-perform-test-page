<!DOCTYPE html>
<html>
<body>

<form action="/protect/a3_post" method="post" autocomplete="on">
	Content
	<input type="text" name="post_content">
	<input type="submit">
</form>

<p>Fill in and submit the form, then reload the page to see how autocomplete works.</p>

<p>Notice that autocomplete is "on" for the form, but "off" for the e-mail field.</p>

</body>
</html>